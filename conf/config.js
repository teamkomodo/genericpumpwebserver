/**
 * Config Files
 */

module.exports = {
  /*
   *
   */
  //defaultController: 'MAIN',

  /*
   *
   */
  controllers: [

	 { host: '10.1.9.12', /*0.49 for test setup*/
	  slot: 1, /* slot 2 for test setup */
      connection_name: 'PUMP9',
      port: 102,
      type: 'nodes7',
      tagfile: './PumpTags.txt' }
  
  ],

  /*
   * Tagsets
   * TagSet names must be predefined here only then can a tag be associated.
   * Otherwise during tag registering process you will receive a TagSet Does Not
   * Exist Error.
   */
  tagsets: [
    'status'
  ],

  /*
   * Warnings
   * Similar to TagSet but only will only be used for warning tags
   */
  warnings: [    
    'master_warnings'
  ],

  logs:  {
	'main':
		{ interval: 1 },
	'secondary':
		{ interval: 5 }
	},
  
  /*
   * Tags
   * All tags that will be used in the application. Only tags registered here
   * and associated to a valid Tagset or Warningset will be available to the 
   * client views.
   * 
   * Options:
   * TODO: create complete list of options
   */
  tags : {  	
    'PUMP9/ANALOG_DB.PUMP_1.DISCH_PRESS':{
      tagsets:['status'],
	  logs:['main'],
	  header:'Discharge 1 Pressure',
	  scaling: 0.145038
    },
	'PUMP9/ANALOG_DB.PUMP_2.DISCH_PRESS':{
      tagsets:['status'],
	  logs:['main'],
	  header:'Discharge 2 Pressure',
	  scaling: 0.145038
    },
	'PUMP9/PUMP_DB.OVERALL.RATE':{
	  tagsets:['status'],
	  logs:['main'],
	  header:'Pump Rate',
	  scaling: 6.29
	},
    'PUMP9/PUMP_DB.OVERALL.SUCTION_FLOWMETER.RATE':{
	  tagsets:['status'],
	  logs:['main'],
	  header:'Pump Flow Meter Rate',
	  scaling: 6.29
	},
	'PUMP9/LOG_DB.JOB_NAME':{
	  tagsets:['status'],
	  logs:['main'],
	  header:'Job Name'
	}
  }
}

/*
,
	'PUMP/ENG_TRANS_DB.ENGINE.SPEED':{
      tagsets:['status'],
	  logs:['secondary'],
	  header:'Engine Speed'	
	},
	'PUMP/ENG_TRANS_DB.TRANS.GEAR_ATT':{
      tagsets:['status'],
	  logs:['secondary'],
	  header:'Trans Gear'	
	}
*/
