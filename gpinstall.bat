@ECHO OFF
@rem This file launches a powershell script with the same base file name as the batch file, with admin rights (will be a UAC prompt usually) and bypassing script execution prevention.
PowerShell.exe -NoProfile -Command "& {Start-Process PowerShell.exe -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File ""%~dpn0.ps1""' -Verb RunAs}"
