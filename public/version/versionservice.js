﻿'use strict';

angular.module('versioninfo', [])

.service('versionService', VersionService)

function VersionService($http) {
    var service = this,

        currentVer = { node: 'Waiting...', app: 'Waiting...', client: 'Waiting...'};
		
		$http.get('/versioninfo').then(function(res) { 
			currentVer.node = res.data.nodever; 
			currentVer.app = res.data.appver;
		}, function(err) { console.log(err); } )

		$http.get('package.json').then(function(res) { 
			currentVer.client = res.data.version;
		}, function(err) { console.log(err); } )

	service.get = function() {
		return currentVer;
	}
}
