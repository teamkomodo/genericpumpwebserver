# Komodo installer PowerShell script
$PB=Split-Path $PSCommandPath
$FWPATH=$PB+"\server.exe"
$LOGPATH=$PB+"\gpinstall.log"
$EXEPATH=$PB+'\GPWeb.exe'
$CODPATH=$PB+'\public\usercontent\datalog'
$COAPATH=$PB+'\public\usercontent\alarmlog'
Start-Transcript -path $LOGPATH 
Write-Output "Chart Software Install Script: $(Get-Date)"
Write-Output "Working Directory: $PB"
Write-Output "Service Executable: $EXEPATH"
Write-Output "Firewall-Exception Executable: $FWPATH"
try
{
    Remove-NetFirewallRule -DisplayName "GPWeb" -ErrorAction Stop
    Write-Output "Successfully removed previous firewall rule."
}
catch
{
    Write-Output "Creating a firewall rule as it did not exist previously."
}
New-NetFirewallRule -DisplayName "GPWeb" -Action Allow -Description "For All Inbound" -Direction Inbound -Profile Domain,Private,Public -Program $FWPATH
try
{
    $GSTASK=Get-ScheduledTask -TaskName "GPCODTask" -ErrorAction Stop
}
catch
{
    Write-Output "Creating a scheduled task to delete old log files."
	$COFARG = '-p ' + $CODPATH + ' -m *.csv /D -365 /C "cmd /c del @path"'
	$action = New-ScheduledTaskAction -Execute 'forfiles.exe' -Argument $COFARG
	$trigger = New-ScheduledTaskTrigger -Daily -At 9am
	Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "GPCODTask" -Description "Cleaning of old log files"

    Write-Output "Creating a scheduled task to delete old alarm files."
	$COFARG = '-p ' + $COAPATH + ' -m *.csv /D -365 /C "cmd /c del @path"'
	$action = New-ScheduledTaskAction -Execute 'forfiles.exe' -Argument $COFARG
	$trigger = New-ScheduledTaskTrigger -Daily -At 10am
	Register-ScheduledTask -Action $action -Trigger $trigger -TaskName "GPCOATask" -Description "Cleaning of old alarm files"
}
& $EXEPATH stop
& $EXEPATH uninstall
& $EXEPATH install
& $EXEPATH start
Stop-Transcript
