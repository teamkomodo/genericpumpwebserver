'use strict';

var trela = angular.module("trela", ['ngRoute','angular-dygraphs','versioninfo']);

trela.config(['$routeProvider', function($routeProvider){
  $routeProvider
    .when('/',{
      templateUrl: "/views/statusonly.html"
    })
}]);

trela.factory('socket', function ($rootScope) {
  var socket = io.connect();
  return {
    hasEvent: function(eventName){
      return socket.$events && socket.$events[eventName];
    },
    on: function (eventName, callback) {
	  var referencePtr = function (arg1) {
        $rootScope.$apply(function () {
          callback.call(socket, arg1, eventName);
        });
      }
      socket.on(eventName, referencePtr);
      return referencePtr;
    },
	remove: function (eventName, callbackReference) {
	  socket.removeListener(eventName, callbackReference);
	},
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

/**
 * Trela Controllers
 *
 */

//function TrelaCtrl($scope, $location, $interval, socket){
trela.controller('TrelaCtrl', function($scope, $location, $interval, socket){

  $scope.disconnected = false;

  $scope.intervalcount = 0;
  
  $scope.write = function(tag, value){
    socket.emit('write', {tag:tag, value:value});
  };
  
  socket.on('disconnect', function(){
    $scope.disconnected = true;
  });

  socket.on('reconnect', function(){
    var currentPath = $location.path();
//    $location.path(currentPath);  // Doesn't seem to always refresh all html/css
// We would like to do the equivalent of "F5" or the browser full refresh button
// at least for the first application of Trela.
// This works in conjunction with "nodemon" which restarts trela and disconnects/reconnects the socket
	location.reload();
    $scope.disconnected = false;
  });
  
  $scope.changeView = function(view){
      $location.path(view); // path not hash
  }
});

trela.controller('TagCtrl', function($scope, $location, $http, $httpParamSerializer, socket, versionService) {
  var annotationCodes = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');
  var annotationSeries = "Main Press"; // annotations require a series
  // Holder for the event
  
  socket.on('reconnect',function(){
	$scope.reloadHistory();
  });
  
  $scope.subscribedTagsets = [];
  $scope.subscribedTagsetFunctions = [];
  
  $scope.version = function() {
			return versionService.get();
  }
  
  $scope.chartparams = {};
  $scope.textboxmaxbbl = 25;
  $scope.textboxmaxpsi = 10000;
  
  // URL for chart
  $scope.chartcsvurl = '/log/main/twohourcsv' + '?' + $httpParamSerializer($scope.chartparams); 

  // Chart stuff
  $scope.datax = [[null,null,null,null,null]];
  $scope.annotationsx = [];
  $scope.legendx = {
    series: {
      "Discharge 1 Pressure": {
        label: "Discharge 1 Pressure"
      },
      "Discharge 2 Pressure": {
        label: "Discharge 2 Pressure"
      },
      "Pump Rate": {
        label: "Pump Rate"
      },
	  "Pump Flow Meter Rate": {
        label: "Pump Flow Meter Rate"
      }
    }
  }

  $scope.actionsx = {
    buttons: {
        Zoom: {
          label: "Zoom",
          asLabel: true
        },
        minute: {
          label: "Minute",
          value: 1000,
          css: "",
          cb: "zoom"
        },
        hour: {
          label: "Hour",
          value: 3600,
          css: "",
          cb: "zoom"
        },
        hour2: {
          label: "2 Hour",
          value: 7200,
          css: "",
          cb: "zoom"
        },
        hour4: {
          label: "4 Hour",
          value: 14400,
          css: "",
          cb: "zoom"
        },
        day: {
          label: "Day",
          value: 86400,
          css: "",
          cb: "zoom"
        },
        reset: {
          label: "Reset",
          value: 0,
          css: "",
          cb: "resetZoom"
        },
        Pan: {
          label: "Pan",
          asLabel: true
        },
        left: {
          label: "Left",
          value: -1,
          css: "",
          cb: "pan",
          disable: "!isXZoomed()"
        },
        right: {
          label: "Right",
          value: +1,
          css: "",
          cb: "pan",
          disable: "!isXZoomed()"
        },
        live: {
          label: "Live",
          css: "",
          cb: "live",
          disable: "!isXZoomed()",
          toggle: "isLiveCss()"
        }
    }
  }

  $scope.optionsx = {
    // drawPoints: true,
    // hideOverlayOnMouseOut: false,
	// These labels have to match the labels in the CSV file or it will not work.
    labels: ["Time","Discharge 1 Pressure","Discharge 2 Pressure","Pump Rate","Pump Flow Meter Rate"],
    connectSeparatedPoints: false,
	//zoom: 3600,
    series: {
      "Discharge 1 Pressure": {
        color: "red"
      },
      "Discharge 2 Pressure": {
        color: "darkred"
      },
      "Pump Rate": {
        axis: "y2",
        color: "blue" // #E8A317
      },
	  "Pump Flow Meter Rate": {
        axis: "y2",
        color: "darkblue" // #E8A317
      }
    },
    labelsSeparateLines:true,
    yRangePad:1, // to make sure the upper yrange is in view
    drawAxesAtZero:true, // to make sure the axes is at zero and not padded :)
    axisLabelWidth:80,
    // legend: "always",
    ylabel: "PSI",
    y2label: "BPM",
    axes: {
      y: {
			valueRange: [0, 10000],
			axisLabelFormatter: function(y) {
				return Math.round(y).toString();
			},
			valueFormatter: function(y) {
				return Math.round(y).toString();
			}
		},
      y2: {
			valueRange: [0, 1500],
			axisLabelFormatter: function(y2) {
				return Math.round(y2).toString();
			},
			valueFormatter: function(y) {
				return parseFloat(y).toFixed(2);
			}
		}
    },
    strokeWidth: 2
    // define color selection here or specifically on series above
    // colors: ["red","darkred","yellow","green","fuchsia","darkmagenta"]
  };

  $scope.rangecallbackx = function(minX, maxX, yRanges) {
	$scope.chartparams.from = minX/1000;
	$scope.chartparams.to = maxX/1000; //moment().unix(maxX/1000);
	$scope.chartcsvurl = '/log/main/pumpcsv' + '?' + $httpParamSerializer($scope.chartparams);	
  };
  

  
  $scope.reloadHistory = function(){

      console.log('reloadHistory');
      var dtFormat = 'MM/DD/YYYY HHmmss';

	  // We pass in the customer name and well ID that we receive live.  This way, when a user changes
	  // the customer or well ID, the chart resets immediately.  Everything is done "by customer and well ID" in this case.

		$scope.chartparams = {
			from:   moment().subtract(24,'hours').unix(),//format(dtFormat),
			to:     moment().unix(),//format(dtFormat),
			format: dtFormat,
			includeheaders: false,
			customer: $scope.status["PUMP9/LOG_DB.JOB_NAME"].value,
			wellid: $scope.status["PUMP9/LOG_DB.JOB_NAME"].value
		};

		$scope.chartcsvurl = '/log/main/pumpcsv' + '?' + $httpParamSerializer($scope.chartparams);
		
		$http({
          url:'/log/main/twohour',
          method:'GET',
          params: $scope.chartparams
        })
        .then(
          function(rawdata) {
            var data = [];
            var anns = []
            var labels = $scope.optionsx.labels;
            var idx = 0;
            rawdata.data.forEach(function(d) {
              data.push(
                [new Date(d[labels[0]]),
                d[labels[1]], //"P1 Pressure"
                d[labels[2]], //"P2 Pressure":
                d[labels[3]],
				d[labels[4]]
              ]);


              if(typeof d["Event"] !== 'undefined' && d["Event"] !== '' && typeof d["Job Total"] !== 'undefined'){
                anns.push(
                  {
                    series: annotationSeries,
                    x: (new Date(d[labels[0]])).getTime(), // has to be a timestamp
//                    shortText: annotationCodes[$scope.annotationsx.length % 26],
                    shortText: annotationCodes[anns.length % 26],
                    cssClass: 'dygraph-annotation',
                    text: d["Event"],
					total: d["Job Total"],
                    attachAtBottom: true
                  }
                );
              }
      		});

            $scope.annotationsx = anns;
            $scope.datax = data;
        });
  
  }

  $scope.$watch('status["PUMP9/LOG_DB.JOB_NAME"].value',function() {
	if ($scope.status.hasOwnProperty("PUMP9/LOG_DB.JOB_NAME")) {
		$scope.customer = $scope.status["PUMP9/LOG_DB.JOB_NAME"].value;
	}
  	if ($scope.status) { // && $scope.status.hasOwnProperty("MAIN/CUSTOMER_NAME")) {
  		$scope.reloadHistory();
  	}
  });
  
  // Set the maximum chart pressure this way.
  /* $scope.$watch('status["MAIN/CHART_MAX_PRESSURE"].value',function() {
    if ($scope.status && $scope.status.hasOwnProperty("MAIN/CHART_MAX_PRESSURE")) {
		$scope.optionsx.axes.y.valueRange = [0, $scope.status["MAIN/CHART_MAX_PRESSURE"].value];
	}  
  }); */
  
  // Set the maximum chart pressure and BBL
  $scope.$watch('textboxmaxbbl', function () {
	  if ($scope.textboxmaxbbl > 0 && $scope.textboxmaxbbl <= 1500) {
		  $scope.optionsx.axes.y2.valueRange = [0, +$scope.textboxmaxbbl];
		  }
    });
  
  $scope.$watch('textboxmaxpsi', function () {
	  if ($scope.textboxmaxpsi > 0 && $scope.textboxmaxpsi <= 10000) {
		  $scope.optionsx.axes.y.valueRange = [0, +$scope.textboxmaxpsi];
		  }
    });
  
  $scope.registerTagSets = function(tagsets){
  var t = [].concat(tagsets);
	var thisDate;
	var skipped;
    jQuery.each(t,function(index,tagset){ // IE8 doesn't do forEach
      $scope[tagset] = $scope[tagset] || {};
      if(socket.hasEvent(tagset)) return;
      $scope.subscribedTagsetFunctions.push(socket.on(tagset, function(data){
		$scope[tagset] = data;
		thisDate = new Date(data["PUMP9/ANALOG_DB.PUMP_1.DISCH_PRESS"].Time);
		if ($scope.lastchange && Math.abs(thisDate - $scope.lastchange) < 1000) {
			// We skip every second point if interval < 1000 at least for now.
			skipped = true;
		} else {
/*      if(data["MAIN/EVENT"].value !== ''){
        $scope.annotationsx.push(
          {
            series: annotationSeries,
            x: data["MAIN/ANALOG_DB.DISCH_PRESS"].Time,
            shortText: annotationCodes[$scope.annotationsx.length % 26],
            cssClass: 'dygraph-annotation',
            text: data["MAIN/EVENT"].value,
			total: data["MAIN/DH_JOB_TOTAL"].value,
            attachAtBottom: true
          }
        );
      }*/

      $scope.datax.push(
        [ new Date(data["PUMP9/ANALOG_DB.PUMP_1.DISCH_PRESS"].Time), // Has to be a date object
          data["PUMP9/ANALOG_DB.PUMP_1.DISCH_PRESS"].value, //"Discharge 1 Pressure"
          data["PUMP9/ANALOG_DB.PUMP_2.DISCH_PRESS"].value, //"Discharge 2 Pressure"
          data["PUMP9/PUMP_DB.OVERALL.RATE"].value, //"Pump Rate"
		  data["PUMP9/PUMP_DB.OVERALL.SUCTION_FLOWMETER.RATE"].value //"Pump Flow Meter Rate"
        ]
      );
      $scope.lastchange = thisDate;
		}

	  }));
	  $scope.subscribedTagsets.push(tagset);
    });
  }

  $scope.$on('$destroy',function(){
	jQuery.each($scope.subscribedTagsets,function(index,tagset){ // IE8 doesn't do forEach
		socket.remove(tagset, $scope.subscribedTagsetFunctions[index]);
	});
  });
});

trela.controller('TagListCtrl',function($scope, socket) {
  $scope.registerTagSets = function(tagsets){
    //TODO: validate tagsets
    var t = [].concat(tagsets); // make sure we have an array
    jQuery.each(t,function(index,tagset){  // IE8 doesn't do forEach
      $scope[tagset] = $scope[tagset] || {};
      if(socket.hasEvent(tagset)) return;
      socket.on(tagset, function(data){
        $scope[tagset] = data;
      });
    });
  }
});

trela.directive('tagsetSubscriptions', function factory() {
  return {
    restrict: 'A',
    link: function(scope, elem, attrs) {
      // TODO: attribute error checking
      scope.registerTagSets(attrs.tagsetSubscriptions.split(','));
    }
  }
});

trela.filter('toNum', function($log) {
  return function(input, decimalplaces) {
    // No Change
    if(typeof decimalplaces === 'undefined'){
      return input;
    }

    // Invalid Decimal Places
    if(decimalplaces < 0 || decimalplaces > 20){
      $log.error('Decimal', decimalplaces, 'Out of Range')
      return 'T_RANG'
    }

    // Contains Alpha characters
    if(typeof input === 'string' && input.match(/[A-Z_-]/gi) !== null){
      return input;
    }

    var n = parseFloat(input);

    // Not a valid number
    if(isNaN(n)){
       return input;
    }

    // Format Decimal Places
    return n.toFixed(decimalplaces);
  }
});


