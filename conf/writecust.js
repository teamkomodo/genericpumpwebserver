// This script file is used to write the "Customer ID" to a test setup for testing purposes without Siemens software.
// Note the absolute addresses on the bottom are not the same as in the production PLC - use with care.
//
// This script is not called by node in production.
	var pccc = require('nodeS7');
	var conn = new pccc;
	var doneReading = false;
	var doneWriting = false;

	conn.initiateConnection({port: 102, host: '10.103.0.12', slot:1}, connected);

	function connected(err) {
		if (typeof(err) !== "undefined") {
			// We have an error.  Maybe the PLC is not reachable.  
			console.log(err);
			process.exit();
		}
		conn.setTranslationCB(tagLookup);

		// conn.addItems(['CUST','ID']);
		// conn.writeItems(['CUST', 'ID'],['No Job','No ID'], valuesWritten);

		conn.addItems(['EVENT']);
		conn.writeItems(['EVENT'],['START SPACER']);

		setTimeout(function() {
			conn.addItems(['EVENT']);
			conn.writeItems(['EVENT'],[''], valuesWritten);	
		}, 1000);
	
	}

	function valuesReady(anythingBad, results) {
		if (anythingBad) { console.log("SOMETHING WENT WRONG READING VALUES!!!!"); } else {console.log(results);}
		//console.log("Value is " + conn.findItem('TEST1').value + " quality is " + conn.findItem('TEST1').quality);
		doneReading = true;
		/*if (doneWriting)*/ { process.exit(); }
	}

	function valuesWritten(anythingBad) {
		if (anythingBad) { console.log("SOMETHING WENT WRONG WRITING VALUES!!!!"); }
		console.log("Done writing.");
		doneWriting = true;
		/*if (doneReading) */{ process.exit(); }
	}

	// This is a very simple "tag lookup" callback function that would eventually be replaced with either a database findOne(), or a large array in memory.  
	// Note that the return value is a controller absolute address and datatype specifier.  
	// If you want to use absolute addresses only, you can do that too.  
	function tagLookup(tag) {
		switch (tag) {
		case 'CUST':
			return 'DB103,S30.20';				// Direct output  
		case 'ID':
			return 'DB103,S52.10';				// String test 
		case 'EVENT':
			return 'DB102,S1500.10';			// String test 
		default:
			return undefined;
		}
	}
