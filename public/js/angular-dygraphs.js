/**
 * dygraph directive for AngularJS
 *
 * Author: Chris Jackson
 *
 * License: MIT
 */
angular.module("angular-dygraphs", [
    'ngSanitize'
])
    .directive('ngDygraphs', ['$window', '$sce', function ($window, $sce) {
        return {
            restrict: 'E',
            scope: { // Isolate scope
                data: '=',
                annotations: '=',
                options: '=',
                legend: '=?',
                actions: '=?',
                height: '@',
				rangecallback: '='
            },
            template: '<div class="ng-dygraphs">' + // Outer div to hold the whole directive

                '<div class="graph-wrapper">' + // Outer Graph Div for dynamic resize
                '<div class="graph"></div>' + // Div for graph
                '</div>' +

                '<div class="legend" ng-if="LegendEnabled">' +          // Div for legend
                    '<div class="series-container">' +                      // Div for series
                        '<div ng-repeat="series in legendSeries" class="series">' +
                            '<a ng-click="selectSeries(series)">' +
                                '<span ng-style="seriesStyle(series)" class="glyphicon" ng-class="{true:\'glyphicon-check\', false:\'glyphicon-unchecked\'}[series.visible]"></span> ' +
                                '<span ng-bind-html="seriesLine(series)"></span>' +
                                '<span ng-style="seriesStyle(series)">{{series.label}}</span>' +
                                '</a>' +
                        '</div>' +                                              // Repeat
                    '</div>' +                                              // Series Div
                '</div>' +                                              // Legend Div

                '<div class="actions">' +
                    '<div ng-repeat="action in actionButtons" class="action">' +
						'<bution ng-disabled="{{action.disable}}" ng-if="!action.asLabel" ng-click="doAction(action)" ng-class="{{action.toggle}}" class="btn btn-default {{action.css}}">' +
							'{{action.label}}' +
							'</bution>' +
						'<label ng-if="action.asLabel">{{action.label}}</label>' +
                    '</div>'+
                '</div>' +

                '</div>',                                               // Outer div
            link: function (scope, element, attrs) {
                scope.LegendEnabled = true;

                var parent = element.parent();
                var mainDiv = element.children()[0];
                var chartDiv = $(mainDiv).children()[0];
                var legendDiv = $(mainDiv).children()[1];

                var graph = scope.graph = new Dygraph(chartDiv, scope.data, scope.options);

                scope.zoomRange = 7200000; // Used to default to 0
                scope.liveView = true; // Now defaults to true.

                scope.$watch(function(){
                    return scope.data.length;
                }, function () {
                    // console.log("data")
                    // var options = scope.options;
                    // if (options === undefined) {
                    //     options = {};
                    // }
                    // options.file = scope.data;
                    // options.highlightCallback = scope.highlightCallback;
                    // options.unhighlightCallback = scope.unhighlightCallback;
                    // if (scope.legend !== undefined) {
                    //     options.labelsDivWidth = 0;
                    // }

					if (!scope.redrawing) {
						scope.redrawing = true;
						//console.log("Annotations:");
						//console.log(scope.annotations);

						if (scope.annotations && scope.annotations.length) {
							graph.setAnnotations(scope.annotations, false);
						}

						// Update Graph Date Window if live is toggled on
						// (Even if the chart does not report we are zoomed - this way we default to 2h on open)
						if(scope.isLive()){
							var e,l;
							e = scope.graph.xAxisExtremes();
							l = e[1] - scope.zoomRange;
							desired_range = [l, e[1] + (e[1] - l)/40 ]; // Dana added the addition of e[1] - l/40 as a hack to get a gap on right. 
							if (typeof moment === "function") {
								desired_range = [l, moment(e[1]).unix()*1000 + (moment(e[1]).unix()*1000 - l)/40 ]; // Dana added the addition of e[1] - l/40 as 
							} else {
								desired_range = [l, e[1]];  // Skip the hack if we're not able to use moment as it causes errors sometimes.
							}
							console.log("Desired Range Pre");
							console.log(desired_range);
							scope.graph.updateOptions({file: scope.data, dateWindow: desired_range});
							console.log("Desired Range Post");
							console.log(desired_range);
							if(typeof scope.rangecallback === "function"){
								console.log("Zoom Range");
								console.log(scope.zoomRange);
								scope.rangecallback(desired_range[0], desired_range[1], undefined);
							}
						} else {
							// In any case we could not do this for S&K - we have skipped doing this for now.
							// DM Jan 2 2018 - graph.updateOptions({file: scope.data});
						}
						setTimeout(function(){ scope.redrawing = false; },300);
					} /* else {
						console.log("Holding off on redraw to prevent performance problems.");
					} */

                });

                scope.$watch("legend", function () {
                    // Clear the legend
                    var colors = graph.getColors();
                    var labels = graph.getLabels();

                    scope.legendSeries = {};

                    if (scope.legend !== undefined && scope.legend.dateFormat === undefined) {
                        scope.legend.dateFormat = 'MMMM Do YYYY, h:mm:ss a';
                    }

                    // If we want our own legend, then create it
                    if (scope.legend !== undefined && scope.legend.series !== undefined) {
                        var cnt = 0;
                        for (var key in scope.legend.series) {
                            scope.legendSeries[key] = {};
                            scope.legendSeries[key].color = colors[cnt];
                            scope.legendSeries[key].label = scope.legend.series[key].label;
                            scope.legendSeries[key].format = scope.legend.series[key].format;
                            scope.legendSeries[key].visible = true;
                            scope.legendSeries[key].column = cnt;

                            cnt++;
                        }
                    }
                });

                scope.$watch("actions", function () {
                    scope.actionsLabel = "Actions:";
                    scope.actionButtons = {};

                    if (scope.actions !== undefined && scope.actions.label){
                        scope.actionsLabel = scope.actions.label;
                    }
                    if (scope.actions !== undefined && scope.actions.buttons !== undefined) {
                        for (var key in scope.actions.buttons) {
                            scope.actionButtons[key] = {};
                            scope.actionButtons[key].label = scope.actions.buttons[key].label;
                            scope.actionButtons[key].value = scope.actions.buttons[key].value;
                            scope.actionButtons[key].css = scope.actions.buttons[key].css;
                            scope.actionButtons[key].cb = scope.actions.buttons[key].cb;
                            scope.actionButtons[key].asLabel = scope.actions.buttons[key].asLabel;
                            scope.actionButtons[key].disable = scope.actions.buttons[key].disable;
                            scope.actionButtons[key].toggle = scope.actions.buttons[key].toggle;
                        }
                    }
                });

                scope.$watch("options", function(newOptions){

                    // register callbacks
                    if(typeof newOptions.zoomCallback === "undefined"){
                        newOptions.zoomCallback = scope.zoomCallback;
                    }

                    // if(typeof newOptions.highlightCallback === undefined){
                    //     newOptions.highlightCallback = scope.highlightCallback;
                    // }

                    // if(typeof newOptions.unhighlightCallback === undefined){
                    //     newOptions.unhighlightCallback = scope.unhighlightCallback;
                    // }

                    graph.updateOptions(newOptions);
                });

                // update the zoomRange with zoom selection
                scope.zoomCallback = function(minX, maxX, yRanges) {
                    // turn of liveView
                    scope.liveView = false;
                    // update zoom
                    var rangeDiff = maxX - minX;
                    scope.zoomRange = rangeDiff;
                    // console.log(scope.liveView);
					if(typeof scope.rangecallback === "function"){
						scope.rangecallback(minX, maxX, yRanges);
					}						
                }

                // scope.highlightCallback = function (event, x, points, row) {
                //     if(!scope.options.showPopover)
                //         return;
                //     var html = "<table><tr><th colspan='2'>";
                //     if (typeof moment === "function" && scope.legend !== undefined) {
                //         html += moment(x).format(scope.legend.dateFormat);
                //     }
                //     else {
                //         html += x;
                //     }
                //     html += "</th></tr>";

                //     angular.forEach(points, function (point) {
                //         var color;
                //         var label;
                //         var value;
                //         if (scope.legendSeries[point.name] !== undefined) {
                //             label = scope.legendSeries[point.name].label;
                //             color = "style='color:" + scope.legendSeries[point.name].color + ";'";
                //             if(scope.legendSeries[point.name].format) {
                //                 value = point.yval.toFixed(scope.legendSeries[point.name].format);
                //             }
                //             else {
                //                 value = point.yval;
                //             }
                //         }
                //         else {
                //             label = point.name;
                //             color = "";
                //         }
                //         html += "<tr " + color + "><td>" + label + "</td>" + "<td>" + value + "</td></tr>";
                //     });
                //     html += "</table>";
                // };

                // scope.unhighlightCallback = function (event, a, b) {
                // };

                scope.seriesLine = function (series) {
                    return $sce.trustAsHtml('<svg height="14" width="20"><line x1="0" x2="16" y1="8" y2="8" stroke="' +
                        series.color + '" stroke-width="3" /></svg>');
                };

                scope.seriesStyle = function (series) {
                    // if (series.visible) {
                    return {color:series.color};
                    // }
                    // return {};
                };

                scope.selectSeries = function (series) {
                    // console.log("Change series", series);
                    series.visible = !series.visible;
                    graph.setVisibility(series.column, series.visible);
                };

                scope.doAction = function (action){
                    if(typeof action !== undefined && typeof action.cb !== undefined){
                        scope[action.cb](action);
                    }
                }

                scope.isLiveCss = function(){
                    return scope.liveView?'active':'';
                }

                scope.isLive = function(){
                    return scope.liveView;
                }

                scope.isXZoomed = function(){
                    return scope.graph.isZoomed('x');
                }

                scope.resetZoom = function(action){
                    scope.zoomRange = 0;
                    scope.graph.resetZoom();
                }

                scope.zoom = function(action){
                    var w = scope.graph.xAxisRange();
                    var offset = action.value * 1000;
                    scope.zoomRange = offset;

                    // If Live then zoom right to left
                    if(scope.isLive()){
                        var e = scope.graph.xAxisExtremes();
                        desired_range = [ e[1] - offset, e[1] ];
                    }else{
                        desired_range = [ w[0], w[0] + offset ];

                    }
                    // console.log(scope.zoomRange);
                    scope.graph.updateOptions({dateWindow: desired_range});
					if(typeof scope.rangecallback === "function"){
						scope.rangecallback(desired_range[0], desired_range[1], undefined);
					}							
                }

                scope.pan = function(action){
                    if(scope.graph.isZoomed('x')){
                        scope.liveView = false;
                        var w = scope.graph.xAxisRange();
                        var e = scope.graph.xAxisExtremes();
                        var scale = w[1] - w[0];
                        var amount = scale * 0.25 * action.value;
                        // console.log(amount);
                        var l = w[0] + amount;
                        var r = w[1] + amount;

                        // clamp pan to data extremes
                        if(amount>0){ // panright
                            if( r > e[1] ){
                                r = e[1];
                                l = e[1] -scope.zoomRange;
                            }
                        }else if(amount< 0){ // panleft
                            if( l < e[0] ){
                                l = e[0];
                                r = e[0] +scope.zoomRange;
                            }
                        }
                        desired_range = [l,r];
                        scope.graph.updateOptions({dateWindow: desired_range});
						if(typeof scope.rangecallback === "function"){
							scope.rangecallback(desired_range[0], desired_range[1], undefined);
						}							
                    }
                }

                scope.live = function(action){
                    if(!scope.liveView){
                        scope.liveView = true;
                        var e = scope.graph.xAxisExtremes();
                        var l = e[1] - scope.zoomRange;
                        desired_range = [l, e[1] ];
                        scope.graph.updateOptions({dateWindow: desired_range});
						if(typeof scope.rangecallback === "function"){
							scope.rangecallback(desired_range[0], desired_range[1], undefined);
						}							
                    }else{
                        scope.liveView = false;
                    }

                }
            }
        };
    }]);
