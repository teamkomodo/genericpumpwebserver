'use strict';

var trela = angular.module("trela", ['ngRoute','ngQuickDate','angularCharts','n3-line-chart']);

trela.config(['$routeProvider', function($routeProvider){
  $routeProvider
    .when('/',{
      templateUrl: "/views/statusonly.html"
    })
    .when('/dtpic',{
      templateUrl: "/views/dtpic.html"
    })	
    .when('/list',{
      templateUrl: "/views/list.html"
    })		
    .when('/list2',{
      templateUrl: "/views/list2.html"
    })
    .when('/historymain',{
      templateUrl: "/views/historymain.html"
    })	
    .when('/nospreadsheet',{
      templateUrl: "/views/statusonly.html"
    })		
}]);

trela.factory('socket', function ($rootScope) {
  var socket = io.connect();
  return {
    hasEvent: function(eventName){
      return socket.$events && socket.$events[eventName];
    },
    on: function (eventName, callback) {
	  var referencePtr = function (arg1) {
        $rootScope.$apply(function () {
          callback.call(socket, arg1, eventName);
        });
      }	
      socket.on(eventName, referencePtr);
      return referencePtr;
    },
	remove: function (eventName, callbackReference) {
	  socket.removeListener(eventName, callbackReference);
	},	
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

/**
 * Trela Controllers
 *
 */

//function TrelaCtrl($scope, $location, $interval, socket){
trela.controller('TrelaCtrl', function($scope, $location, $interval, socket){

  $scope.disconnected = false;

  $scope.intervalcount = 0;
  
  socket.on('disconnect', function(){
    $scope.disconnected = true;
  }); 

  socket.on('reconnect', function(){
    var currentPath = $location.path();
//    $location.path(currentPath);  // Doesn't seem to always refresh all html/css
// We would like to do the equivalent of "F5" or the browser full refresh button
// at least for the first application of Trela.  
// This works in conjunction with "nodemon" which restarts trela and disconnects/reconnects the socket
	location.reload();
    $scope.disconnected = false;
  });

  $scope.changeView = function(view){
      $location.path(view); // path not hash
  }  
  
 /* $interval(function(){
	if ($scope.pagecode > 0) {
		// Currently the case with index2.html and pagecode is 1
	    if ($location.path() == '/') { 
			$scope.intervalcount++;
			if ($scope.intervalcount >= 1) {  // 4 = 2 minutes - use this to display one view longer.  With 1, no effect.
				$scope.intervalcount = 0;
				$scope.changeView('/dtpic');
			}
		} else if ($location.path() == '/dtpic') {  // for a while this was != /historymain
			$scope.changeView('/list') 
		} else if ($location.path() == '/list') {  // for a while this was != /historymain
			$scope.changeView('/list2') 
		} else if ($location.path() == '/list2') {  // for a while this was != /historymain
			$scope.changeView('/nospreadsheet') 
		} else if ($location.path() == '/nospreadsheet') {  // for a while this was != /historymain
			$scope.changeView('/') 
		}
	} else {
		// Normal case
	    if ($location.path() == '/') { 
			$scope.intervalcount++;
			if ($scope.intervalcount >= 1) {  // 4 = 2 minutes - use this to display one view longer.  With 1, no effect.
				$scope.intervalcount = 0;
				$scope.changeView('/dtpic');
			}
		} else if ($location.path() == '/dtpic') {  // for a while this was != /historymain
			$scope.changeView('/list') 
		} else if ($location.path() == '/list') {  // for a while this was != /historymain
			$scope.changeView('/list2') 
		} else if ($location.path() == '/list2') {  // for a while this was != /historymain
			$scope.changeView('/') 
		}
	}
	
  },45000);  // 30000  */
});


trela.controller('TagCtrl', function($scope, $location, $http, socket) {
  // Holder for the event 
  $scope.subscribedTagsets = [];
  $scope.subscribedTagsetFunctions = [];
  
  // Chart stuff
  $scope.datax = [

/*{Time: new Date(), "P1 Pressure": 0, "P2 Rate": 0, "P2 Pressure": 0, "P1 Rate": 0},
      {Time: new Date(), "P1 Pressure": 0.706, "P2 Rate": -2.794, "P2 Pressure": 6.182, "P1 Rate": -10.731}	 */ 
  ];

  $scope.optionsx = {
	axes: {
        x: {
          type: "date",
          key: "Time",
		  color: "white",
		  grid: true
        },
        y: {type: "linear", grid: true, ticks: [0,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000]},
		y2: {type: "linear", grid: true, ticks: [0,2,4,6,8,10,12,14,16,18,20]}
      },
      series: [
        {
			y: "P1 Pressure",
			label: "Pump 1 Pressure",
			color: "red",
			type: "line",
			thickness: "2px",
			drawDots: false
        },
        {
			y: "P2 Pressure",
			label: "Pump 2 Pressure",
			color: "darkred",
			type: "line",
			thickness: "2px",
			drawDots: false
        },
        {
			y: "P1 Rate",
			label: "Pump 1 Rate",
			color: "yellow",
			axis: "y2",
			type: "line",
			thickness: "2px",
			drawDots: false
        },
        {
			y: "P2 Rate",
			label: "Pump 2 Rate",
			color: "green",
			axis: "y2",
			type: "line",
			thickness: "2px",
			drawDots: false
        },
        {
			y: "DH Density",
			label: "DH Density",
			color: "fuchsia",
			axis: "y2",
			type: "line",
			thickness: "2px",
			drawDots: false
        },
        {
			y: "Recirc Density",
			label: "Recirc Density",
			color: "darkmagenta",
			axis: "y2",
			type: "line",
			thickness: "2px",
			drawDots: false
        }			
      ],
      lineMode: "linear",
      tension: 0.7,
//      tooltip: {mode: 'axes', interpolate: false}
      tooltip: {mode: 'none'}
  };
  
  $scope.reloadHistory = function(){
  
      var dtFormat = 'MM/DD/YYYY HHmmss';
	  
      var params = {
          from:   moment().subtract(1,'h').format(dtFormat),
          to:     moment().format(dtFormat),
          format: dtFormat,
		  customer: $scope.status["MAIN/CUSTOMER_NAME"].value,
		  wellid: $scope.status["MAIN/WELL_ID"].value
		  /*,
          fields: $scope.fieldHeaders */
        };
          
		console.log("PARAMS");
		console.log(params);
		
		$http({
          url:'/log/main/bywell',
          method:'GET',
          params: params
        })
        .success(
          function(data) {
			$scope.datax = data;
			//console.log($scope.datax);
			//console.log("above is datax");
			data.forEach(function(d) {
				d.Time = new Date(d.Time);
			});
        });
  }
  
  $scope.$watch('status["MAIN/CUSTOMER_NAME"].value + status["MAIN/WELL_ID"].value',function() { 
	if ($scope.status && $scope.status.hasOwnProperty("MAIN/CUSTOMER_NAME")) {
//	typeof(status["MAIN/CUSTOMER_NAME"].value) !== 'undefined' && typeof(status["MAIN/WELL_ID"].value) !== 'undefined') {
		$scope.reloadHistory(); 
	}
  });
  
  //$scope.reloadHistory();
  
  // End chart stuff

  $scope.registerTagSets = function(tagsets){
    var t = [].concat(tagsets);
	var thisDate;
	var skipped;
    jQuery.each(t,function(index,tagset){ // IE8 doesn't do forEach
      $scope[tagset] = $scope[tagset] || {};
      if(socket.hasEvent(tagset)) return;
      $scope.subscribedTagsetFunctions.push(socket.on(tagset, function(data){ 
		$scope[tagset] = data;
		thisDate = new Date(data["MAIN/PUMP1_PRESSURE"].Time);
		if ($scope.lastchange && Math.abs(thisDate - $scope.lastchange) < 1000) {
			skipped = true;
			console.log("skipping");
		} else {
			console.log(thisDate);
			$scope.datax.push({Time: new Date(data["MAIN/PUMP1_PRESSURE"].Time), 
				"P1 Pressure": data["MAIN/PUMP1_PRESSURE"].value, 
				"P2 Pressure": data["MAIN/PUMP2_PRESSURE"].value, 
				"P1 Rate": data["MAIN/PUMP1_RATE"].value, 
				"P2 Rate": data["MAIN/PUMP2_RATE"].value,
				"Recirc Density": data["MAIN/RECIRC_DENSITY"].value,
				"DH Density": data["MAIN/DH_DENSITY"].value
				});
			$scope.lastchange = thisDate;
		}

	  }));
	  $scope.subscribedTagsets.push(tagset);
    });
  }
  
  $scope.$on('$destroy',function(){ 
	jQuery.each($scope.subscribedTagsets,function(index,tagset){ // IE8 doesn't do forEach
		socket.remove(tagset, $scope.subscribedTagsetFunctions[index]);
	});
  });
});

trela.controller('TagListCtrl',function($scope, socket) {
  $scope.registerTagSets = function(tagsets){
    //TODO: validate tagsets
    var t = [].concat(tagsets); // make sure we have an array
    jQuery.each(t,function(index,tagset){  // IE8 doesn't do forEach
      $scope[tagset] = $scope[tagset] || {};
      if(socket.hasEvent(tagset)) return;
      socket.on(tagset, function(data){
        $scope[tagset] = data;
      });
    });
  }
});

trela.controller('HistoryCtrl',function($scope, $http) {
    $scope.oborFile = '';
    $scope.fromDate;
    $scope.toDate;
    $scope.fieldHeaders = 'Time,Downtime Counter';
    // move into service
    var dtFormat = 'MM/DD/YYYY HHmmss';

	$scope.chartType = 'line';
	
	$scope.config = {
		title: 'Test Title II', // chart title. If this is false, no title element will be created.
		tooltips: true,
		labels: false, // labels on data points
		// exposed events
		mouseover: function() {},
		mouseout: function() {},
		click: function() {},
		// legend config
		legend: {
		display: true, // can be either 'left' or 'right'.
		position: 'left',
		// you can have html in series name
		htmlEnabled: false
		},
		// override this array if you're not happy with default colors
		colors: [],
		innerRadius: 0, // Only on pie Charts
		lineLegend: 'lineEnd', // Only on line Charts
		lineCurveType: 'cardinal', // change this as per d3 guidelines to avoid smoothline
		isAnimate: true, // run animations while rendering chart
//		xAxisTickFormat: 's', //refer tickFormats in d3 to edit this value
		xAxisMaxTicks: 7, // Optional: maximum number of X axis ticks to show if data points exceed this number
		yAxisTickFormat: 's', // refer tickFormats in d3 to edit this value
		waitForHeightAndWidth: false // if true, it will not throw an error when the height or width are not defined (e.g. while creating a modal form), and it will be keep watching for valid height and width values
	};
	
	$scope.acdata = {
		series: ["Sales", "Income", "Expense"],
		data: [{
			x: [10,20,30],
			y: [54, 0, 879],
			tooltip: "This is a tooltip"
		}]
	};
	
    $scope.refreshObor = function(){

      if($scope.fromDate && $scope.toDate){
        var params = {
          from:   moment($scope.fromDate).format(dtFormat),
          to:     moment($scope.toDate).format(dtFormat),
          format: dtFormat,
          fields: $scope.fieldHeaders
        };

        $http({
          url:'/log/main/lastminute',
          method:'GET',
          params: params
        })
        .success(
          function(data) {
          console.log("returning");
		  console.log(data);
		  console.log(data.data);
		  $scope.testdata = data;
		  console.log($scope.testdata);
		  console.log("above is testdata");
/*          if(typeof data.data === "string"){
            //XXX how best to make the relative to site?
            $scope.oborFile = data.data.replace('public','');
          }*/

        });   
      }
    }
});

trela.directive('tagsetSubscriptions', function factory() {
  return {
    restrict: 'A',
    link: function(scope, elem, attrs) {
      // TODO: attribute error checking
      scope.registerTagSets(attrs.tagsetSubscriptions.split(','));
    }
  }
});

trela.directive('tag', function factory(){
 return {
  restrict: 'A',
  scope:{
    tag:"@",
    tagset:"="
  },
  template: "{{ tagset[tag].value }}"
 } 
});

trela.filter('toNum', function($log) {
  return function(input, decimalplaces) {
    // No Change
    if(typeof decimalplaces === 'undefined'){
      return input;
    }

    // Invalid Decimal Places
    if(decimalplaces < 0 || decimalplaces > 20){
      $log.error('Decimal', decimalplaces, 'Out of Range')
      return 'T_RANG'
    }

    // Contains Alpha characters
    if(typeof input === 'string' && input.match(/[A-Z_-]/gi) !== null){
      return input;
    }

    var n = parseFloat(input);
    
    // Not a valid number
    if(isNaN(n)){
       return input;
    }

    // Format Decimal Places
    return n.toFixed(decimalplaces);
  }
});


trela.directive('liveLineChart', function factory(){
  
  return {
  restrict: 'E',

  // set up the isolate scope so that we don't clobber parent scope
  scope: {
    bind:        '=',
  },

  link: function(scope, element, attrs) {

    var margin = {top: 20, right: 80, bottom: 30, left: 50},
    width = 960 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

    // are we using interpolation
    var interpolate = attrs.interpolate || 'false';

    var label = attrs.label || 'Change';

    // create x,y sclaes (x is inferred as time)
    var x = d3.time.scale()
    .range([0, width])

    var y = d3.scale.linear()
    .range([height, 0]);

    // create line generator 
    var line = d3.svg.line()
    .x(function(d) { return x(d.time); })
    .y(function(d) { return y(d.value); });

    // create x,y axis 
    var xAxis = d3.svg.axis()
    .scale(x)
    .orient('bottom')
    .ticks(d3.time.second, 1)

    var yAxis = d3.svg.axis()
    .scale(y)
    .orient('left');

    // create the root SVG node
    var svg = d3.select(element[0]).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // generate the line. Data is empty at link time
    var lines = [];
    var colors = d3.scale.category10();
    for (var n in scope.bind) {
      var l = svg.append('path')
      .datum([])
      .attr('class', 'line')
      .style('stroke', colors(n))
      .attr('d', line)
      lines.push(l);
    }

    // insert the x axis (no data yet)
    svg.append('g')
    .attr('class', 'area x axis ')
    .attr('transform', 'translate(0,' + height + ')')
    .call(xAxis);

    // insert the y axis (no data yet)
    svg.append('g')
    .attr('class', 'area y axis ')
    .call(yAxis)
    .append('text')
    .attr('transform', 'rotate(-90)')
    .attr('y', 6)
    .attr('dy', '.71em')
    .style('text-anchor', 'end')
    .text('Values');

    scope.$watch('bind', function(data) {
      // just because scope is bound doesn't imply we have data.
      if (data) {

        // pull the data array from the facet
        data = data || [];

        // use that data to build valid x,y ranges
        x.domain(d3.extent(data[0].data, function(d) { return d.time; }));
        y.domain([0,d3.max(data, function(obj) {
          return d3.max(obj.data, function(d){
            return d.value; 
          });
        })])

        // create the transition 
        var t = svg.transition().duration(0);

        // feed the current data to our area/line generators
        for( l in lines){
          lines[l].attr('d', line(data[l].data));
        }

        // update our x,y axis based on new data values
        t.select('.x').call(xAxis);
        t.select('.y').call(yAxis);
      }
    }, true)
  }
}
});


trela.directive('testLineChart', function factory(){
  
  return {
  restrict: 'E',

  // set up the isolate scope so that we don't clobber parent scope
  scope: {
    bind:        '=',
  },

  link: function(scope, element, attrs) {

    var margin = {top: 20, right: 80, bottom: 30, left: 50},
    width = 960 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

    // are we using interpolation
    var interpolate = attrs.interpolate || 'false';

    var label = attrs.label || 'Change';

    // create x,y sclaes (x is inferred as time)
    var x = d3.time.scale()
    .range([0, width])

    var y = d3.scale.linear()
    .range([height, 0]);

    // create line generator 
    var line = d3.svg.line()
    .x(function(d) { return x(d.time); })
    .y(function(d) { return y(d.value); });

    // create x,y axis 
    var xAxis = d3.svg.axis()
    .scale(x)
    .orient('bottom')
    .ticks(d3.time.second, 1)

    var yAxis = d3.svg.axis()
    .scale(y)
    .orient('left');

    // create the root SVG node
    var svg = d3.select(element[0]).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // generate the line. Data is empty at link time
    var lines = [];
    var colors = d3.scale.category10();
    for (var n in scope.bind) {
      var l = svg.append('path')
      .datum([])
      .attr('class', 'line')
      .style('stroke', colors(n))
      .attr('d', line)
      lines.push(l);
    }

    // insert the x axis (no data yet)
    svg.append('g')
    .attr('class', 'area x axis ')
    .attr('transform', 'translate(0,' + height + ')')
    .call(xAxis);

    // insert the y axis (no data yet)
    svg.append('g')
    .attr('class', 'area y axis ')
    .call(yAxis)
    .append('text')
    .attr('transform', 'rotate(-90)')
    .attr('y', 6)
    .attr('dy', '.71em')
    .style('text-anchor', 'end')
    .text('Values');

    scope.$watch('bind', function(data) {
      // just because scope is bound doesn't imply we have data.
      if (data) {

        // pull the data array from the facet
        data = data || [];

        // use that data to build valid x,y ranges
        x.domain(d3.extent(data[0].data, function(d) { return d.time; }));
        y.domain([0,d3.max(data, function(obj) {
          return d3.max(obj.data, function(d){
            return d.value; 
          });
        })])

        // create the transition 
        var t = svg.transition().duration(0);

        // feed the current data to our area/line generators
        for( l in lines){
          lines[l].attr('d', line(data[l].data));
        }

        // update our x,y axis based on new data values
        t.select('.x').call(xAxis);
        t.select('.y').call(yAxis);
      }
    }, true)
  }
}
});


/**
 *
 * TSV line Chart
 *
 */
trela.directive('tsvLineChart', function factory() {
//  'use strict';
  return {
    restrict: 'E',

    // set up the isolate scope so that we don't clobber parent scope
    scope: {
      tsv:  '@'
    },

    link: function(scope, element, attrs) {

      var margin = {top: 20, right: 80, bottom: 30, left: 50},
      width = 960 - margin.left - margin.right,
      height = 400 - margin.top - margin.bottom;

      var x = d3.time.scale()
      .range([0, width]);

      var y = d3.scale.linear()
      .range([height, 0]);

      var color = d3.scale.category20();

      var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

      var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left");

      var line = d3.svg.line()
      .interpolate("linear")
      .x(function(d) {
        return x(d.timestamp);
      })
      .y(function(d) { 
        return y(+d.value);
      });


      var svg = d3.select(element[0]).append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // insert the x axis (no data yet)
      svg.append('g')
      .attr('class', 'area x axis ')
      .attr('transform', 'translate(0,' + height + ')')
      .call(xAxis);

      // insert the y axis (no data yet)
      svg.append('g')
      .attr('class', 'y axis ')
      .call(yAxis)
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '.71em')
      .style('text-anchor', 'end')
      .text("values");

      scope.$watch('tsv', function(tsv) {
        if(tsv.indexOf('.tsv')== -1){
          return;
        }
        d3.tsv(tsv, function(error, data) {
          color.domain(d3.keys(data[0]).filter(function(key) { return key !== "timestamp"; }));

          var groups = color.domain().map(function(name) {
            return {
              name: name,
              values: data.map(function(d) {
                return {timestamp: +moment(d.timestamp, 'DD/MM/YYYY HHmmss'), value: d[name]};
              })
            };
          });

          x.domain(d3.extent(data, function(d) { 
            return +moment(d.timestamp, 'DD/MM/YYYY HHmmss'); 
          }));

          
          y.domain([
            d3.min(groups, function(c) { return d3.min(c.values, function(v) { return +v.value; }); }),
            d3.max(groups, function(c) { return d3.max(c.values, function(v) { return +v.value; }); })
          ]);
          
          svg.selectAll(".group").remove();

          var group = svg.selectAll(".group")
          .data(groups)
          .enter().append("g")
          .attr("class", "group");

          group.append("path")
          .attr("class", "line")
          .attr("d", function(d) {return line(d.values); })
          .style("stroke", function(d) { return color(d.name); });

          var t = svg.transition().duration(0);
          t.select('.x').call(xAxis);
          t.select('.y').call(yAxis);

        });
      });
    }
  }
});

/**
 *
 * TSV line Chart
 *
 */
trela.directive('tvLineChart', function factory($window) {
//  'use strict';
  return {
    restrict: 'E',

    // set up the isolate scope so that we don't clobber parent scope
    scope: {
      bind:  '@'
    },

    link: function(scope, element, attrs) {

      var margin = {top: 20, right: 80, bottom: 30, left: 50},
      width = Math.max(960,961) - margin.left - margin.right,
      height = Math.max(400,element[0].clientHeight) - margin.top - margin.bottom;

	  	console.log("Width is " + width);
	  
      var x = d3.time.scale()
      .range([0, width]);

      var y = d3.scale.linear()
      .range([height, 0]);

      var color = d3.scale.category20();

      var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

      var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left");

      var line = d3.svg.line()
      .interpolate("linear")
      .x(function(d) {
        return x(+d.timestamp);
      })
      .y(function(d) { 
        return y(+d.value);
      });

	function drawGraph(){
		console.log(d3.select('svg'));
		d3.select('svg').remove();
		d3.select('svg').remove();
		console.log("width " + scope.windowWidth + "ht" + height);
	var svg = d3.select(element[0]).append("svg")
      .attr("width", scope.windowWidth - margin.left - margin.right)
      .attr("height", scope.windowHeight - margin.top - margin.bottom)
//      .attr("width", width + margin.left + margin.right)
//      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // insert the x axis (no data yet)
      svg.append('g')
      .attr('class', 'area x axis ')
      .attr('transform', 'translate(0,' + height + ')')
      .call(xAxis);

      // insert the y axis (no data yet)
      svg.append('g')
      .attr('class', 'y axis ')
      .call(yAxis)
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '.71em')
      .style('text-anchor', 'end')
      .text("values");
	}
	drawGraph();
	
      scope.onResizeFunction = function() {
        scope.windowHeight = $window.innerHeight;
        scope.windowWidth = $window.innerWidth;

		width = Math.max(960,scope.windowWidth) - margin.left - margin.ght,
		height = Math.max(400,scope.windowHeight) - margin.top - margin.bottom;
		
        console.log(scope.windowHeight+" and width is "+scope.windowWidth);
		drawGraph();
      };

      // Call to the function when the page is first loaded
      scope.onResizeFunction();

      angular.element($window).bind('resize', function() {
        scope.onResizeFunction();
        scope.$apply();
      });	  
	   
	  
	  
	  
      scope.$watch('bind', function(data) {
		data = JSON.parse(data);
		console.log("waaa");
		console.log(data);
		console.log("waaa2");
      /*  if(tsv.indexOf('.tsv')== -1){
          return;
        }*/
        /*d3.tsv(tsv, function(error, data)*/ if (data) {
			console.log(d3.keys(data[0]));
			console.log(data[0]);
			color.domain(d3.keys(data[0]).filter(function(key) { return key !== "Time"; }));

		  			console.log(color.domain());

          var groups = color.domain().map(function(name) {
			return {
              name: name,
              values: data.map(function(d) {
//                return {timestamp: +moment(d.Time, 'DD/MM/YYYY HHmmss'), value: d[name]};
                return {timestamp: +moment(d.Time, "YYYY-MM-DD HH:mm:ss"), value: d[name]};
//                return {Time: d.Time, value: d[name]};
              })
            };
          });

          x.domain(d3.extent(data, function(d) { 
		  console.log(moment(d.Time, "YYYY-MM-DD HH:mm:ss").toDate());
            return +moment(d.Time , 'YYYY-MM-DD HH:mm:ss').toDate(); 
          }));

			console.log("X domain is ");
console.log(x.domain());
          
          y.domain([
            d3.min(groups, function(c) { return d3.min(c.values, function(v) { return +v.value; }); }),
            d3.max(groups, function(c) { return d3.max(c.values, function(v) { return +v.value; }); })
          ]);
          
			console.log("Y domain is ");

		  console.log(y.domain());
		  
          svg.selectAll(".group").remove();

          var group = svg.selectAll(".group")
          .data(groups)
          .enter().append("g")
          .attr("class", "group");

          group.append("path")
          .attr("class", "line")
          .attr("d", function(d) {return line(d.values); })
          .style("stroke", function(d) { return color(d.name); });

          var t = svg.transition().duration(0);
          t.select('.x').call(xAxis);
          t.select('.y').call(yAxis);

        }/*);*/
      });
    }
  }
});

